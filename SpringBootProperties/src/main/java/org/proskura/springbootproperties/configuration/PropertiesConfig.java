package org.proskura.springbootproperties.configuration;

import org.proskura.springbootproperties.properties.UserProperties;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class PropertiesConfig {

    @Bean
    @ConfigurationProperties(prefix = "usr")
    public UserProperties getUserProperties (){
        return new UserProperties();
    }
}
