package org.proskura.springbootproperties;

import org.proskura.springbootproperties.properties.UserProperties;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.ConfigurationPropertiesScan;
import org.springframework.core.env.Environment;

import javax.annotation.PostConstruct;

@SpringBootApplication
@ConfigurationPropertiesScan("org.proskura.springbootproperties.properties")
public class SpringBootPropertiesApp {

    @Autowired
    private Environment env;

    @Autowired
    private UserProperties userProperties;

    public static void main(String[] args) {
        SpringApplication.run(SpringBootPropertiesApp.class, args);
    }

    @PostConstruct
    private void checkProperties() {
        System.out.println(userProperties);
    }
}
